﻿using ApartmaniReact.DB;
using ProjekatWEB.Models;
using ProjekatWEB.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApartmaniReact.Controllers
{
    public class AdministratorController : ApiController
    {
        DataBaseProvider dataBaseProvider = new DataBaseProvider();

        [HttpGet]
        [Route("api/administrator/prikazLicnihPodataka")]
        public Administrator PrikaziPodatke(string korisnickoIme)
        {
            Administrator admin = dataBaseProvider.GetAllAdministrators().Find(x => x.KorisnickoIme == korisnickoIme);
            
            return admin;
        }

        [HttpGet]
        [Route("api/administrator/izmenaLicnihPodataka")]
        public string IzmeniPodatke(string korisnickoIme, string lozinka, string ime, string prezime, string pol)
        {
            Administrator administrator = dataBaseProvider.GetAllAdministrators().Find(x => x.KorisnickoIme == korisnickoIme);

            administrator.Lozinka = lozinka;
            administrator.Ime = ime;
            administrator.Prezime = prezime;
            administrator.Pol = pol;
            administrator.Uloga = KorisnickeUloge.Administrator;

            dataBaseProvider.UpdateAdministratorToDatabase(administrator);

            return "Podaci uspesno izmenjeni";
        }

        private List<KorisnikViewModel> GetKorisnici()
        {
            List<KorisnikViewModel> korisnici = new List<KorisnikViewModel>();

            //ADMINI
            List<Administrator> adminiList = dataBaseProvider.GetAllAdministrators();
            foreach (Administrator admin in adminiList)
            {
                KorisnikViewModel kvm = new KorisnikViewModel()
                {
                    Id = admin.Id,
                    KorisnickoIme = admin.KorisnickoIme,
                    Ime = admin.Ime,
                    Prezime = admin.Prezime,
                    Pol = admin.Pol,
                    Uloga = "Administrator",
                    Blokiran = admin.Blokiran
                };
                korisnici.Add(kvm);
            }
            //DOMACINI
            List<Domacin> domaciniList = dataBaseProvider.GetAllDomacin();
            foreach (Domacin admin in domaciniList)
            {
                KorisnikViewModel kvm = new KorisnikViewModel()
                {
                    Id = admin.Id,
                    KorisnickoIme = admin.KorisnickoIme,
                    Ime = admin.Ime,
                    Prezime = admin.Prezime,
                    Pol = admin.Pol,
                    Uloga = "Domacin",
                    Blokiran = admin.Blokiran
                };
                korisnici.Add(kvm);
            }
            //GOSTI
            List<Gost> gostiList = dataBaseProvider.GetAllGost();
            foreach (Gost admin in gostiList)
            {
                KorisnikViewModel kvm = new KorisnikViewModel()
                {
                    Id = admin.Id,
                    KorisnickoIme = admin.KorisnickoIme,
                    Ime = admin.Ime,
                    Prezime = admin.Prezime,
                    Pol = admin.Pol,
                    Uloga = "Gost",
                    Blokiran = admin.Blokiran
                };
                korisnici.Add(kvm);
            }
            return korisnici;
        }

        [HttpGet]
        [Route("api/administrator/prikaziSveKorisnike")]
        public List<KorisnikViewModel> PrikaziSveKorisnike()
        {
            return GetKorisnici();
        }

        [HttpGet]
        [Route("api/administrator/ptertaziKorisnike")]
        public List<KorisnikViewModel> PtertaziKorisnike(bool ulogaFlag, string ulogaVr, bool polFlag, string polVr, bool korImeFlag, string korisnickoIme)
        {
            List<KorisnikViewModel> filtriraniKorisnici = new List<KorisnikViewModel>();
            List<KorisnikViewModel> korisnici = GetKorisnici();

            if (ulogaFlag && polFlag && korImeFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Uloga == ulogaVr && x.Pol == polVr && x.KorisnickoIme == korisnickoIme).ToList();
            else if (ulogaFlag && polFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Uloga == ulogaVr && x.Pol == polVr).ToList();
            else if (ulogaFlag && korImeFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Uloga == ulogaVr && x.KorisnickoIme == korisnickoIme).ToList();
            else if (polFlag && korImeFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Pol == polVr && x.KorisnickoIme == korisnickoIme).ToList();
            else if (ulogaFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Uloga == ulogaVr).ToList();
            else if (polFlag)
                filtriraniKorisnici = korisnici.Where(x => x.Pol == polVr).ToList();
            else if (korImeFlag)
                filtriraniKorisnici = korisnici.Where(x => x.KorisnickoIme == korisnickoIme).ToList();

            return filtriraniKorisnici;
        }

        [HttpGet]
        [Route("api/administrator/dodajDomacina")]
        public string DodajDomacina(string korisnickoIme, string lozinka, string ime, string prezime, string pol)
        {
            bool flag = true;

            List<Administrator> adminiList = dataBaseProvider.GetAllAdministrators();
            Dictionary<string, Administrator> adminiDict = new Dictionary<string, Administrator>();
            foreach (Administrator admin in adminiList)
                adminiDict.Add(admin.KorisnickoIme, admin);

            List<Domacin> domaciniList = dataBaseProvider.GetAllDomacin();
            Dictionary<string, Domacin> domaciniDict = new Dictionary<string, Domacin>();
            foreach (Domacin domacin in domaciniList)
                domaciniDict.Add(domacin.KorisnickoIme, domacin);

            List<Gost> gostiList = dataBaseProvider.GetAllGost();
            Dictionary<string, Gost> gostiDict = new Dictionary<string, Gost>();
            foreach (Gost gost in gostiList)
                gostiDict.Add(gost.KorisnickoIme, gost);

            if (gostiDict.ContainsKey(korisnickoIme) || domaciniDict.ContainsKey(korisnickoIme) || adminiDict.ContainsKey(korisnickoIme))
                flag = false;

            if (flag)
            {
                Domacin dodatDomacin = new Domacin(korisnickoIme, lozinka, ime, prezime, pol.ToString(), KorisnickeUloge.Domacin);
                dataBaseProvider.AddDomacinToDatabase(dodatDomacin);
                return "Domacin uspesno dodat";
            }
            else
                return "Korisnicko ime vec postoji u bazi.";
        }

        [HttpGet]
        [Route("api/administrator/prikaziSveApartmane")]
        public List<ApartmanName_IDViewModel> PrikaziSveApartmane()
        {
            List<Domacin> domacini = dataBaseProvider.GetAllDomacin();
            List<Apartman> apartmen = dataBaseProvider.GetAllApartmans();
            List<ApartmanName_IDViewModel> app = new List<ApartmanName_IDViewModel>();
            foreach (Apartman a in apartmen)
            {
                if (a.Obrisan == false)
                {
                    app.Add(new ApartmanName_IDViewModel() { Id = a.Id, Naziv = a.Naziv });
                }
            }
            return app;
        }

        [HttpGet]
        [Route("api/administrator/kombinovanaPretragaApartmana")]
        public List<ApartmanName_IDViewModel> PrikaziApartmanSort(DateTime datumDolaska, DateTime datumOdlaska, string grad, /*string drzava, */double cenaOd, double cenaDo, int brojSobaMIN, int brojSobaMAX, int brojOsobaMIN, int brojOsobaMAX)
        {
            List<Apartman> apartmen = dataBaseProvider.GetAllApartmans();
            List<Apartman> apartmenDatumStatus = new List<Apartman>();
            List<Apartman> apartmenFilter = new List<Apartman>();
            List<ApartmanName_IDViewModel> app = new List<ApartmanName_IDViewModel>();
            List<Domacin> domacini = dataBaseProvider.GetAllDomacin();

            foreach (Apartman a in apartmen)
            {
                //if (a.StatusApartmana == StatusApartmana.Aktivno)
                {
                    //if (a.Domacin.KorisnickoIme == korisnickoIme && a.StatusApartmana == StatusApartmana.Aktivno)
                    if (a.Obrisan == false)
                    {
                        int resultDolazak = DateTime.Compare(a.PocetniDatumIzdavanja.Date, datumDolaska.Date);
                        int resultOdlazak = DateTime.Compare(a.KrajnjiDatumIzdavanja.Date, datumOdlaska.Date);

                        if (DateTime.Compare(datumDolaska, DateTime.Parse("1/1/2010")) == 0)
                            resultDolazak = 0;
                        if (DateTime.Compare(datumOdlaska, DateTime.Parse("1/1/2010")) == 0)
                            resultOdlazak = 0;

                        if ((resultDolazak < 0 || resultDolazak == 0) && (resultOdlazak > 0 || resultOdlazak == 0))
                        {
                            apartmenDatumStatus.Add(a);
                        }
                    }
                }
            }

            apartmenFilter = apartmenDatumStatus.Where(x => /*x.Lokacija.Adresa.NaseljenoMesto == grad && */x.CenaPoNoci >= cenaOd && x.CenaPoNoci <= cenaDo && x.BrojSoba >= brojSobaMIN && x.BrojSoba <= brojSobaMAX && x.BrojGostiju >= brojOsobaMIN && x.BrojGostiju <= brojOsobaMAX).ToList();

            if (grad != null)
            {
                foreach (Apartman a in apartmenFilter)
                    if (a.Lokacija.Adresa.NaseljenoMesto.Contains(grad))
                        app.Add(new ApartmanName_IDViewModel() { Id = a.Id, Naziv = a.Naziv });
            }
            else
            {
                foreach (Apartman a in apartmenFilter)
                    app.Add(new ApartmanName_IDViewModel() { Id = a.Id, Naziv = a.Naziv });
            }

            return app;
        }

        [HttpGet]
        [Route("api/administrator/filtriranjeApartmana")]
        public List<ApartmanName_IDViewModel> FiltrirajApartman(string parametarFiltriranja, string tipApartmana, string status)
        {
            List<Apartman> apartmen = dataBaseProvider.GetAllApartmans();
            List<Apartman> filtriraniApartmani = new List<Apartman>();

            /*if (parametarFiltriranja == "SadrzajApartmana")
            {
                Dictionary<string, SadrzajApartmana> sadrzajAppDict;

                foreach (Apartman app in apartmen)
                {
                    sadrzajAppDict = new Dictionary<string, SadrzajApartmana>();

                    foreach (SadrzajApartmana sadrzajApp in app.Sadrzaj)
                        sadrzajAppDict.Add(sadrzajApp.Naziv, sadrzajApp);

                    bool flag = true;
                    foreach (string sadrzajFilter in favm.Sadrzaj)
                    {
                        if (!sadrzajAppDict.ContainsKey(sadrzajFilter))
                            flag = false;
                    }
                    if (flag)
                        filtriraniApartmani.Add(app);
                }
            }
            else */
            if (parametarFiltriranja == "TipApartmana")
            {
                if (tipApartmana == "CeoApartman")
                    filtriraniApartmani = apartmen.Where(x => x.Tip == TipApartmana.CeoApartman).ToList();
                else
                    filtriraniApartmani = apartmen.Where(x => x.Tip == TipApartmana.Soba).ToList();
            }
            else
            {
                if (status == "Aktivno")
                    filtriraniApartmani = apartmen.Where(x => x.StatusApartmana == StatusApartmana.Aktivno).ToList();
                else
                    filtriraniApartmani = apartmen.Where(x => x.StatusApartmana == StatusApartmana.Neaktivno).ToList();
            }

            List<ApartmanName_IDViewModel> apps = new List<ApartmanName_IDViewModel>();
            foreach (Apartman apartman in filtriraniApartmani)
                if (apartman.Obrisan == false)
                    apps.Add(new ApartmanName_IDViewModel() { Id = apartman.Id, Naziv = apartman.Naziv });

            return apps;
        }

        [HttpGet]
        [Route("api/administrator/sortiranjeApartmana")]
        public List<ApartmanName_IDViewModel> PrikazSortiranihApartmana(string nacinSortiranja)
        {
            List<Domacin> domacini = dataBaseProvider.GetAllDomacin();
            List<Apartman> apartmen = dataBaseProvider.GetAllApartmans();

            List<Apartman> sortiraniApp = new List<Apartman>();
            if (nacinSortiranja == "CenaRastuca")
                sortiraniApp = apartmen.OrderBy(o => o.CenaPoNoci).ToList();
            else
                sortiraniApp = apartmen.OrderByDescending(o => o.CenaPoNoci).ToList();

            List<ApartmanName_IDViewModel> app = new List<ApartmanName_IDViewModel>();
            foreach (Apartman a in sortiraniApp)
            {
                if (a.Obrisan == false)
                {
                    app.Add(new ApartmanName_IDViewModel() { Id = a.Id, Naziv = a.Naziv });
                }
            }

            return app;
        }

        [HttpGet]
        [Route("api/administrator/detaljiApartmana")]
        public ApartmanViewModel PrikaziApartman(int id)
        {
            List<Gost> gosti = dataBaseProvider.GetAllGost();
            List<Apartman> apartmens = dataBaseProvider.GetAllApartmans();
            Dictionary<int, Apartman> apartmanDict = new Dictionary<int, Apartman>();
            foreach (Apartman app in apartmens)
                apartmanDict.Add(app.Id, app);
            ApartmanViewModel avm = new ApartmanViewModel();
            try
            {
                Apartman appart = apartmanDict[id];

                avm = new ApartmanViewModel()
                {
                    Id = appart.Id,
                    Naziv = appart.Naziv,
                    Tip = appart.Tip.ToString(),
                    Komentari = new List<KomentarApartmanaViewModel>(),
                    BrojSoba = appart.BrojSoba,
                    BrojGostiju = appart.BrojGostiju,
                    Mesto = appart.Lokacija.Adresa.NaseljenoMesto,
                    Adresa = appart.Lokacija.Adresa.Ulica,
                    PostanskiBroj = appart.Lokacija.Adresa.PostanskiBroj,
                    PocetniDatumIzdavanja = appart.PocetniDatumIzdavanja,
                    KrajnjiDatumIzdavanja = appart.KrajnjiDatumIzdavanja,
                    CenaPoNoci = appart.CenaPoNoci,
                    VremePrijave = appart.VremePrijave.ToString("HH:mm"),
                    VremeOdjave = appart.VremeOdjave.ToString("HH:mm"),
                    StatusApartmana = appart.StatusApartmana.ToString(),
                    Sadrzaj = new List<SadrzajApartmana>(),
                    Rezervacije = new List<RezervacijaViewModel>(),
                    PocetniDatumIzdavanjaStr = String.Format("{0:dd/MM/yyyy}", appart.PocetniDatumIzdavanja),
                    KrajnjiDatumIzdavanjaStr = String.Format("{0:dd/MM/yyyy}", appart.KrajnjiDatumIzdavanja),
                    Slike = new string[appart.Slike.Count]
                };

                foreach (KomentarApartmana kom in appart.Komentari)
                {
                    avm.Komentari.Add(new KomentarApartmanaViewModel()
                    {
                        KorisnickoIme = kom.Gost.KorisnickoIme,
                        Tekst = kom.Tekst,
                        Ocena = kom.Ocena
                    });
                }

                foreach (SadrzajApartmana sadrzaj in appart.Sadrzaj)
                {
                    avm.Sadrzaj.Add(new SadrzajApartmana()
                    {
                        Naziv = sadrzaj.Naziv
                    });
                }

                foreach (Rezervacija rez in appart.Rezervacije)
                {
                    avm.Rezervacije.Add(new RezervacijaViewModel()
                    {
                        PocetniDatum = rez.PocetniDatum.ToString(),
                        KrajnjiDatum = rez.KrajnjiDatum.ToString(),
                        BrojNocenja = rez.BrojNocenja,
                        UkupnaCena = rez.UkupnaCena,
                        KorisnickoIme = rez.Gost.KorisnickoIme,
                        Ime = rez.Gost.Ime,
                        Prezime = rez.Gost.Prezime,
                        Status = rez.Status.ToString()
                    });
                }

                List<string> slikas = new List<string>();
                foreach (Slika slika in appart.Slike)
                {
                    slikas.Add(slika.Photo);
                }
                avm.Slike = slikas.ToArray();
            }
            catch { }

            return avm;
        }

        [HttpGet]
        [Route("api/administrator/brisanjeApartmana")]
        public string BrisanjeApartmana(int id)
        {
            Apartman app = dataBaseProvider.GetAllApartmans().Find(x => x.Id == id);

            app.Obrisan = true;
            app.StatusApartmana = StatusApartmana.Neaktivno;
            dataBaseProvider.UpdateApartmanToDatabase(app);

            return "Apartman uspesno obrisan";
        }

        [HttpGet]
        [Route("api/administrator/izmenaApartmana")]
        public string IzmenaApartmana(string naziv, string status, string tip, int brojSoba, int brojGostiju, int cena, DateTime vremePrijave, DateTime vremeOdjave)
        {
            if (!ModelState.IsValid)
                return "Polja forme nisu popunjena validno.";

            //List<Apartman> apatrmanList = dataBaseProvider.GetAllApartmans();
            //Dictionary<string, Apartman> apartmans = new Dictionary<string, Apartman>();
            //foreach (Apartman a in apatrmanList)
            //    apartmans.Add(a.Naziv, a);

            //Apartman app = apartmans[apartman.Naziv];

            Apartman app = dataBaseProvider.GetAllApartmans().Find(x => x.Naziv == naziv);


            if (tip == "Soba")
                app.Tip = TipApartmana.Soba;
            else
                app.Tip = TipApartmana.CeoApartman;

            app.BrojSoba = brojSoba;
            app.BrojGostiju = brojGostiju;
            app.CenaPoNoci = cena;
            app.VremePrijave = vremePrijave;
            app.VremeOdjave = vremeOdjave;

            //if (apartman.Slike.Length > 0)
            //{
            //    app.Slike = new List<Slika>();
            //    foreach (string slika in apartman.Slike)
            //        app.Slike.Add(new Slika() { Photo = slika });
            //}

            if (status == "Aktivno")
                app.StatusApartmana = StatusApartmana.Aktivno;
            else
                app.StatusApartmana = StatusApartmana.Neaktivno;

            dataBaseProvider.UpdateApartmanToDatabase(app);

            return "Podaci uspesno izmenjeni";
        }

        [HttpGet]
        [Route("api/administrator/prikaziSveRezervacije")]
        public List<RezervacijaViewModel> PrikaziSveRezervacije()
        {
            List<Gost> gosti = dataBaseProvider.GetAllGost();
            List<Domacin> domacini = dataBaseProvider.GetAllDomacin();
            List<Apartman> apartmani = dataBaseProvider.GetAllApartmans();
            List<Rezervacija> rezervacijas = dataBaseProvider.GetAllRezervacije();
            List<RezervacijaViewModel> rvm = new List<RezervacijaViewModel>();
            foreach (Rezervacija r in rezervacijas)
            {
                if (r.Apartman.Obrisan == false)
                {
                    string status = "";
                    if (r.Status == StatusRezervacije.Kreirana)
                        status = "Kreirana";
                    else if (r.Status == StatusRezervacije.Odbijena)
                        status = "Odbijena";
                    else if (r.Status == StatusRezervacije.Odustanak)
                        status = "Odustanak";
                    else if (r.Status == StatusRezervacije.Prihvacena)
                        status = "Prihvacena";
                    else if (r.Status == StatusRezervacije.Zavrsena)
                        status = "Zavrsena";

                    rvm.Add(new RezervacijaViewModel()
                    {
                        Id = r.Id,
                        PocetniDatum = String.Format("{0:dd/MM/yyyy}", r.PocetniDatum),
                        KrajnjiDatum = String.Format("{0:dd/MM/yyyy}", r.KrajnjiDatum),
                        BrojNocenja = r.BrojNocenja,
                        UkupnaCena = r.UkupnaCena,
                        NazivApartmana = r.Apartman.Naziv,
                        Status = status,
                        KorisnickoIme = r.Gost.KorisnickoIme,
                        KorisnickoImeDomacina = r.Apartman.Domacin.KorisnickoIme
                    });
                }

            }
            return rvm;
        }

        [HttpGet]
        [Route("api/administrator/filtriranjeRezervacija")]
        public List<RezervacijaViewModel> PrikaziSveRezervacijeFilter(string ParametarFiltriranja)
        {
            List<Gost> gosti = dataBaseProvider.GetAllGost();
            List<Domacin> domacini = dataBaseProvider.GetAllDomacin();
            List<Apartman> apartmani = dataBaseProvider.GetAllApartmans();
            List<Rezervacija> rezervacijas = dataBaseProvider.GetAllRezervacije();
            List<RezervacijaViewModel> rvm = new List<RezervacijaViewModel>();

            StatusRezervacije statusRezervacije = 0;
            if (ParametarFiltriranja == "Kreirana")
                statusRezervacije = StatusRezervacije.Kreirana;
            else if (ParametarFiltriranja == "Odbijena")
                statusRezervacije = StatusRezervacije.Odbijena;
            else if (ParametarFiltriranja == "Odustanak")
                statusRezervacije = StatusRezervacije.Odustanak;
            else if (ParametarFiltriranja == "Prihvacena")
                statusRezervacije = StatusRezervacije.Prihvacena;
            else if (ParametarFiltriranja == "Zavrsena")
                statusRezervacije = StatusRezervacije.Zavrsena;

            foreach (Rezervacija r in rezervacijas)
            {
                if (r.Apartman.Obrisan == false)
                {
                    if (r.Status == statusRezervacije)
                    {
                        string status = "";
                        if (r.Status == StatusRezervacije.Kreirana)
                            status = "Kreirana";
                        else if (r.Status == StatusRezervacije.Odbijena)
                            status = "Odbijena";
                        else if (r.Status == StatusRezervacije.Odustanak)
                            status = "Odustanak";
                        else if (r.Status == StatusRezervacije.Prihvacena)
                            status = "Prihvacena";
                        else if (r.Status == StatusRezervacije.Zavrsena)
                            status = "Zavrsena";

                        rvm.Add(new RezervacijaViewModel()
                        {
                            Id = r.Id,
                            PocetniDatum = String.Format("{0:dd/MM/yyyy}", r.PocetniDatum),
                            KrajnjiDatum = String.Format("{0:dd/MM/yyyy}", r.KrajnjiDatum),
                            BrojNocenja = r.BrojNocenja,
                            UkupnaCena = r.UkupnaCena,
                            NazivApartmana = r.Apartman.Naziv,
                            Status = status,
                            KorisnickoIme = r.Gost.KorisnickoIme,
                            KorisnickoImeDomacina = r.Apartman.Domacin.KorisnickoIme
                        });
                    }
                }
            }
            return rvm;
        }

        [HttpGet]
        [Route("api/administrator/prikaziDostupanSadrzaj")]
        public List<Sadrzaj> PrikaziDostupanSadrzaj()
        {
            List<Sadrzaj> dostupanSadrzaj = new List<Sadrzaj>();
            List<Sadrzaj> sadrzaj = dataBaseProvider.GetAllSadrzajApartmana();

            foreach (Sadrzaj s in sadrzaj)
                if (!s.Obrisan)
                    dostupanSadrzaj.Add(s);

            return dostupanSadrzaj;
        }

        [HttpGet]
        [Route("api/administrator/brisanjeSadrzaja")]
        public string BrisanjeSadrzaja(int id)
        {
            Sadrzaj sad = dataBaseProvider.GetAllSadrzajApartmana().Find(x => x.Id == id);
            sad.Obrisan = true;
            string naziv = sad.Naziv;
            dataBaseProvider.UpdateSadrzajToDatabase(sad);

            List<SadrzajApartmana> sadrzajApp = dataBaseProvider.GetAllSadrzajApartmanRelacionaTabela();
            foreach (SadrzajApartmana s in sadrzajApp)
            {
                if (s.Naziv == naziv)
                {
                    s.Obrisan = true;
                    dataBaseProvider.UpdateSadrzajApartmanRelacionaTabelaToDatabase(s);
                }
            }

            return "Sadrzaj uspesno obrisan";
        }

        [HttpGet]
        [Route("api/administrator/izmenaSadrzaja")]
        public string IzmenaSadrzaja(int id, string naziv)
        {
            Sadrzaj sad = dataBaseProvider.GetAllSadrzajApartmana().Find(x => x.Id == id);
            sad.Naziv = naziv;
            dataBaseProvider.UpdateSadrzajToDatabase(sad);

            return "Sadrzaj uspesno izmenjen.";
        }

        [HttpGet]
        [Route("api/administrator/dodajPraznicniDatum")]
        public string DodajPraznicniDatum(DateTime PraznicniDatum)
        {
            List<PraznicniDatum> praznicniDatumi = dataBaseProvider.GetAllPraznicniDatumi();

            foreach (PraznicniDatum pd in praznicniDatumi)
            {
                if (DateTime.Compare(pd.Datum, PraznicniDatum) == 0)
                    return "Datum vec postoji.";
            }
            PraznicniDatum praznicniDatum = new PraznicniDatum() { Datum = PraznicniDatum };
            dataBaseProvider.AddPraznicniDatumToDatabase(praznicniDatum);

            return "Datum uspesno dodat.";
        }



    }
}
