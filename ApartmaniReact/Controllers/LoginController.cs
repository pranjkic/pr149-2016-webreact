﻿using ApartmaniReact.DB;
using ProjekatWEB.Models;
using ProjekatWEB.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApartmaniReact.Controllers
{
    public class LoginController : ApiController
    {
        DataBaseProvider dataBaseProvider = new DataBaseProvider();

        [HttpPost]
        [Route("api/login/login")]
        public string Login([FromBody] LoginViewModel korisnik)
        {
            string tipKorisnika = "NE POSTOJI";

            try
            {
                Administrator admin = dataBaseProvider.GetAllAdministrators().Find(x => x.KorisnickoIme == korisnik.KorisnickoIme);
                if (admin.Blokiran == true)
                    return "Korisnik je blokiran.";
                if (admin.Lozinka != korisnik.Lozinka)
                    return "Pogresna lozinka.";
                tipKorisnika = "Administrator";
            }
            catch
            {
                try
                {
                    Domacin domacin = dataBaseProvider.GetAllDomacin().Find(x => x.KorisnickoIme == korisnik.KorisnickoIme);
                    if (domacin.Blokiran == true)
                        return "Korisnik je blokiran.";
                    if (domacin.Lozinka != korisnik.Lozinka)
                        return "Pogresna lozinka.";
                    tipKorisnika = "Domacin";
                }
                catch
                {
                    try
                    {
                        Gost gost = dataBaseProvider.GetAllGost().Find(x => x.KorisnickoIme == korisnik.KorisnickoIme);
                        if (gost.Blokiran == true)
                            return "Korisnik je blokiran.";
                        if (gost.Lozinka != korisnik.Lozinka)
                            return "Pogresna lozinka.";
                        tipKorisnika = "Gost";
                    }
                    catch
                    {
                        tipKorisnika = "Korisnik ne postoji.";
                    }
                }
            }


            return tipKorisnika;
        }

    }
}
