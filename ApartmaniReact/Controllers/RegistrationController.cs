﻿using ApartmaniReact.DB;
using ApartmaniReact.Models.ViewModels;
using ProjekatWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApartmaniReact.Controllers
{
    public class RegistrationController : ApiController
    {
        DataBaseProvider dataBaseProvider = new DataBaseProvider();

        [HttpPost]
        [Route("api/registration/register")]
        public string Register([FromBody] RegisterUserViewModel registerUser)
        {
            bool flag = true;

            List<Administrator> adminiList = dataBaseProvider.GetAllAdministrators();
            Dictionary<string, Administrator> adminiDict = new Dictionary<string, Administrator>();
            foreach (Administrator admin in adminiList)
                adminiDict.Add(admin.KorisnickoIme, admin);

            List<Domacin> domaciniList = dataBaseProvider.GetAllDomacin();
            Dictionary<string, Domacin> domaciniDict = new Dictionary<string, Domacin>();
            foreach (Domacin domacin in domaciniList)
                domaciniDict.Add(domacin.KorisnickoIme, domacin);

            List<Gost> gostiList = dataBaseProvider.GetAllGost();
            Dictionary<string, Gost> gostiDict = new Dictionary<string, Gost>();
            foreach (Gost gost in gostiList)
                gostiDict.Add(gost.KorisnickoIme, gost);

            if (gostiDict.ContainsKey(registerUser.KorisnickoIme) || domaciniDict.ContainsKey(registerUser.KorisnickoIme) || adminiDict.ContainsKey(registerUser.KorisnickoIme))
                flag = false;

            if (flag)
            {
                Gost dodatGost = new Gost(registerUser.KorisnickoIme, registerUser.Lozinka, registerUser.Ime, registerUser.Prezime, registerUser.Pol.ToString(), KorisnickeUloge.Gost);
                dataBaseProvider.AddGostToDatabase(dodatGost);
            }
            else
                return "Korisnicko ime vec postoji u bazi.";

            return "Korisnik uspesno registrovan";
        }
    }
}
