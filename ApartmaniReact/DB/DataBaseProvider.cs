﻿using ProjekatWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmaniReact.DB
{
    public class DataBaseProvider
    {
        ProjectDBContext projectDBContext = new ProjectDBContext();

        public void AddAdministratorToDatabase(Administrator administrator)
        {
            projectDBContext.Administrators.Add(administrator);
            projectDBContext.SaveChanges();
        }
        public void UpdateAdministratorToDatabase(Administrator administrator)
        {
            projectDBContext.Entry(administrator).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }
        public List<Administrator> GetAllAdministrators()
        {
            List<Administrator> administrators = projectDBContext.Administrators.ToList();
            return administrators;
        }

        public void AddDomacinToDatabase(Domacin domacin)
        {
            projectDBContext.Domacins.Add(domacin);
            projectDBContext.SaveChanges();
        }
        public void UpdateDomacinToDatabase(Domacin domacin)
        {
            projectDBContext.Entry(domacin).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }
        public List<Domacin> GetAllDomacin()
        {
            List<Apartman> apartmens = GetAllApartmans();
            List<Domacin> domacins = projectDBContext.Domacins.ToList();
            return domacins;
        }

        public void AddGostToDatabase(Gost gost)
        {
            projectDBContext.Gosts.Add(gost);
            projectDBContext.SaveChanges();
        }
        public void UpdateGostToDatabase(Gost gost)
        {
            projectDBContext.Entry(gost).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }
        public List<Gost> GetAllGost()
        {
            List<Gost> gosts = projectDBContext.Gosts.ToList();
            return gosts;
        }

        public void UpdateApartmanToDatabase(Apartman apartman)
        {
            projectDBContext.Apartmans.Update(apartman);
            projectDBContext.SaveChanges();
        }
        public List<Apartman> GetAllApartmans()
        {
            List<KomentarApartmana> komentars = projectDBContext.Komentars.ToList();
            List<Slika> slikas = projectDBContext.Slikas.ToList();
            List<SadrzajApartmana> sadrzajs = projectDBContext.Sadrzajs.ToList();
            List<Rezervacija> rezervacijas = projectDBContext.Rezervacijas.ToList();
            List<Adresa> adresas = projectDBContext.Adresas.ToList();
            List<Lokacija> lokacijas = projectDBContext.Lokacijas.ToList();
            List<Apartman> apartmens = projectDBContext.Apartmans.ToList();
            return apartmens;
        }
        public void AddApartmanToDatabase(Apartman apartman)
        {
            projectDBContext.Apartmans.Add(apartman);
            projectDBContext.SaveChanges();
        }
        public void DeleteApartmanFromDatabase(Apartman apartman)
        {
            projectDBContext.Apartmans.Remove(apartman);
            projectDBContext.SaveChanges();
        }

        public List<Sadrzaj> GetAllSadrzajApartmana()
        {
            List<Sadrzaj> sadrzajs = projectDBContext.SadrzajApartmanas.ToList();
            return sadrzajs;
        }
        public void AddSadrzajToDatabase(Sadrzaj sadrzaj)
        {
            projectDBContext.SadrzajApartmanas.Add(sadrzaj);
            projectDBContext.SaveChanges();
        }
        public void DeleteSadrzajFromDatabase(Sadrzaj sadrzaj)
        {
            projectDBContext.SadrzajApartmanas.Remove(sadrzaj);
            projectDBContext.SaveChanges();
        }
        public void UpdateSadrzajToDatabase(Sadrzaj sadrzaj)
        {
            projectDBContext.Entry(sadrzaj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }


        public List<SadrzajApartmana> GetAllSadrzajApartmanRelacionaTabela()
        {
            List<SadrzajApartmana> sadrzajs = projectDBContext.Sadrzajs.ToList();
            return sadrzajs;
        }
        public void UpdateSadrzajApartmanRelacionaTabelaToDatabase(SadrzajApartmana sadrzaj)
        {
            projectDBContext.Entry(sadrzaj).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }


        public List<Rezervacija> GetAllRezervacije()
        {
            List<Rezervacija> rezervacijas = projectDBContext.Rezervacijas.ToList();
            return rezervacijas;
        }
        public void UpdateRezervacijaToDatabase(Rezervacija rezervacija)
        {
            projectDBContext.Entry(rezervacija).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }

        public void UpdateKomentarToDatabase(KomentarApartmana komentar)
        {
            projectDBContext.Entry(komentar).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            projectDBContext.SaveChanges();
        }
        public List<KomentarApartmana> GetAllKomentariApartmana()
        {
            List<KomentarApartmana> komentars = projectDBContext.Komentars.ToList();
            return komentars;
        }

        public void AddPraznicniDatumToDatabase(PraznicniDatum praznicniDatum)
        {
            projectDBContext.PraznicniDatumi.Add(praznicniDatum);
            projectDBContext.SaveChanges();
        }
        public List<PraznicniDatum> GetAllPraznicniDatumi()
        {
            List<PraznicniDatum> praznicniDatumi = projectDBContext.PraznicniDatumi.ToList();
            return praznicniDatumi;
        }
    }
}