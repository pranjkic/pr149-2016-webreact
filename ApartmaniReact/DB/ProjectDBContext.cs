﻿using Microsoft.EntityFrameworkCore;
using ProjekatWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmaniReact.DB
{
    public class ProjectDBContext : DbContext
    {
        public ProjectDBContext() : base()
        {

        }

        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Domacin> Domacins { get; set; }
        public DbSet<Gost> Gosts { get; set; }

        public DbSet<Apartman> Apartmans { get; set; }
        public DbSet<KomentarApartmana> Komentars { get; set; }
        public DbSet<Slika> Slikas { get; set; }
        public DbSet<SadrzajApartmana> Sadrzajs { get; set; }
        public DbSet<Rezervacija> Rezervacijas { get; set; }
        public DbSet<Adresa> Adresas { get; set; }
        public DbSet<Lokacija> Lokacijas { get; set; }

        public DbSet<Sadrzaj> SadrzajApartmanas { get; set; }

        public DbSet<PraznicniDatum> PraznicniDatumi { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ApartmaniDatabase;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            optionsBuilder.UseSqlServer(connectionString);
        }
    }
}