﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApartmaniReact.Migrations
{
    public partial class initialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Administrators",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KorisnickoIme = table.Column<string>(nullable: true),
                    Lozinka = table.Column<string>(nullable: true),
                    Ime = table.Column<string>(nullable: true),
                    Prezime = table.Column<string>(nullable: true),
                    Pol = table.Column<string>(nullable: true),
                    Uloga = table.Column<int>(nullable: false),
                    Blokiran = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Adresas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ulica = table.Column<string>(nullable: true),
                    NaseljenoMesto = table.Column<string>(nullable: true),
                    PostanskiBroj = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adresas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Domacins",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KorisnickoIme = table.Column<string>(nullable: true),
                    Lozinka = table.Column<string>(nullable: true),
                    Ime = table.Column<string>(nullable: true),
                    Prezime = table.Column<string>(nullable: true),
                    Pol = table.Column<string>(nullable: true),
                    Uloga = table.Column<int>(nullable: false),
                    Blokiran = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domacins", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Gosts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KorisnickoIme = table.Column<string>(nullable: true),
                    Lozinka = table.Column<string>(nullable: true),
                    Ime = table.Column<string>(nullable: true),
                    Prezime = table.Column<string>(nullable: true),
                    Pol = table.Column<string>(nullable: true),
                    Uloga = table.Column<int>(nullable: false),
                    Blokiran = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gosts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PraznicniDatumi",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Datum = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PraznicniDatumi", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SadrzajApartmanas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(nullable: true),
                    Obrisan = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SadrzajApartmanas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lokacijas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GeografskaSirina = table.Column<double>(nullable: false),
                    GeografskaDuzina = table.Column<double>(nullable: false),
                    Drzava = table.Column<string>(nullable: true),
                    AddrId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lokacijas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lokacijas_Adresas_AddrId",
                        column: x => x.AddrId,
                        principalTable: "Adresas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Apartmans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(nullable: true),
                    Tip = table.Column<int>(nullable: false),
                    BrojSoba = table.Column<int>(nullable: false),
                    BrojGostiju = table.Column<int>(nullable: false),
                    LokacijaId = table.Column<int>(nullable: true),
                    PocetniDatumIzdavanja = table.Column<DateTime>(nullable: false),
                    KrajnjiDatumIzdavanja = table.Column<DateTime>(nullable: false),
                    DomacinId = table.Column<int>(nullable: false),
                    CenaPoNoci = table.Column<double>(nullable: false),
                    VremePrijave = table.Column<DateTime>(nullable: false),
                    VremeOdjave = table.Column<DateTime>(nullable: false),
                    StatusApartmana = table.Column<int>(nullable: false),
                    Obrisan = table.Column<bool>(nullable: false),
                    GostId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Apartmans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Apartmans_Domacins_DomacinId",
                        column: x => x.DomacinId,
                        principalTable: "Domacins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Apartmans_Gosts_GostId",
                        column: x => x.GostId,
                        principalTable: "Gosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Apartmans_Lokacijas_LokacijaId",
                        column: x => x.LokacijaId,
                        principalTable: "Lokacijas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Komentars",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    GostId = table.Column<int>(nullable: false),
                    ApartmanId = table.Column<int>(nullable: false),
                    Tekst = table.Column<string>(nullable: true),
                    Ocena = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Komentars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Komentars_Apartmans_ApartmanId",
                        column: x => x.ApartmanId,
                        principalTable: "Apartmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Komentars_Gosts_GostId",
                        column: x => x.GostId,
                        principalTable: "Gosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rezervacijas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AppId = table.Column<int>(nullable: false),
                    PocetniDatum = table.Column<DateTime>(nullable: false),
                    BrojNocenja = table.Column<int>(nullable: false),
                    UkupnaCena = table.Column<double>(nullable: false),
                    GostId = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    KrajnjiDatum = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rezervacijas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Rezervacijas_Apartmans_AppId",
                        column: x => x.AppId,
                        principalTable: "Apartmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rezervacijas_Gosts_GostId",
                        column: x => x.GostId,
                        principalTable: "Gosts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Sadrzajs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SadrzajId = table.Column<int>(nullable: false),
                    Naziv = table.Column<string>(nullable: true),
                    Obrisan = table.Column<bool>(nullable: false),
                    ApartmanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sadrzajs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sadrzajs_Apartmans_ApartmanId",
                        column: x => x.ApartmanId,
                        principalTable: "Apartmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Slikas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Photo = table.Column<string>(nullable: true),
                    ApartmanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Slikas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Slikas_Apartmans_ApartmanId",
                        column: x => x.ApartmanId,
                        principalTable: "Apartmans",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Apartmans_DomacinId",
                table: "Apartmans",
                column: "DomacinId");

            migrationBuilder.CreateIndex(
                name: "IX_Apartmans_GostId",
                table: "Apartmans",
                column: "GostId");

            migrationBuilder.CreateIndex(
                name: "IX_Apartmans_LokacijaId",
                table: "Apartmans",
                column: "LokacijaId");

            migrationBuilder.CreateIndex(
                name: "IX_Komentars_ApartmanId",
                table: "Komentars",
                column: "ApartmanId");

            migrationBuilder.CreateIndex(
                name: "IX_Komentars_GostId",
                table: "Komentars",
                column: "GostId");

            migrationBuilder.CreateIndex(
                name: "IX_Lokacijas_AddrId",
                table: "Lokacijas",
                column: "AddrId");

            migrationBuilder.CreateIndex(
                name: "IX_Rezervacijas_AppId",
                table: "Rezervacijas",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_Rezervacijas_GostId",
                table: "Rezervacijas",
                column: "GostId");

            migrationBuilder.CreateIndex(
                name: "IX_Sadrzajs_ApartmanId",
                table: "Sadrzajs",
                column: "ApartmanId");

            migrationBuilder.CreateIndex(
                name: "IX_Slikas_ApartmanId",
                table: "Slikas",
                column: "ApartmanId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Administrators");

            migrationBuilder.DropTable(
                name: "Komentars");

            migrationBuilder.DropTable(
                name: "PraznicniDatumi");

            migrationBuilder.DropTable(
                name: "Rezervacijas");

            migrationBuilder.DropTable(
                name: "SadrzajApartmanas");

            migrationBuilder.DropTable(
                name: "Sadrzajs");

            migrationBuilder.DropTable(
                name: "Slikas");

            migrationBuilder.DropTable(
                name: "Apartmans");

            migrationBuilder.DropTable(
                name: "Domacins");

            migrationBuilder.DropTable(
                name: "Gosts");

            migrationBuilder.DropTable(
                name: "Lokacijas");

            migrationBuilder.DropTable(
                name: "Adresas");
        }
    }
}
