﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Administrator : Korisnik
    {
        public int Id { get; set; }
        public Administrator() { }

        public Administrator(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga) : base(korisnickoIme, lozinka, ime, prezime, pol, uloga)
        {
        }

        //public override string ToString()
        //{
        //    return KorisnickoIme+"|"+Lozinka + "|" +Ime + "|" +Prezime + "|" +Pol+"|Administrator";
        //}
    }
}