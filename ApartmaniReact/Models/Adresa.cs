﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Adresa
    {
        public int Id { get; set; }
        public string Ulica { get; set; }
        public string NaseljenoMesto { get; set; }
        public int PostanskiBroj { get; set; }

        public Adresa() { }

        public Adresa(string ulica, string naseljenoMesto, int postanskiBroj)
        {
            Ulica = ulica;
            NaseljenoMesto = naseljenoMesto;
            PostanskiBroj = postanskiBroj;
        }
    }
}