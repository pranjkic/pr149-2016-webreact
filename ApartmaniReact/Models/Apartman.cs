﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Apartman
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public TipApartmana Tip { get; set; }
        public int BrojSoba { get; set; }
        public int BrojGostiju { get; set; }
        public Lokacija Lokacija { get; set; }
        public DateTime PocetniDatumIzdavanja { get; set; }
        public DateTime KrajnjiDatumIzdavanja { get; set; }

        [ForeignKey("Domacin"), Column("DomacinId")]
        public int DomacinId { get; set; }
        public Domacin Domacin { get; set; }

        public ICollection<KomentarApartmana> Komentari { get; set; }
        public ICollection<Slika> Slike { get; set; }

        public double CenaPoNoci { get; set; }
        public DateTime VremePrijave { get; set; }
        public DateTime VremeOdjave { get; set; }
        public StatusApartmana StatusApartmana { get; set; }
        public ICollection<SadrzajApartmana> Sadrzaj { get; set; }
        public ICollection<Rezervacija> Rezervacije { get; set; }
        public bool Obrisan { get; set; }

        public Apartman()
        {
            Lokacija = new Lokacija();
            PocetniDatumIzdavanja = DateTime.Now;
            KrajnjiDatumIzdavanja = DateTime.Now;

            Domacin = new Domacin();
            Komentari = new List<KomentarApartmana>();
            Slike = new List<Slika>();
            Sadrzaj = new List<SadrzajApartmana>();
            Rezervacije = new List<Rezervacija>();
        }

        public Apartman(TipApartmana tip, int brojSoba, int brojGostiju, Lokacija lokacija, List<Vreme> datumiZaIzdavanje, List<Vreme> dostupnostPoDatumima, Domacin domacin, List<KomentarApartmana> komentari, List<Slika> slike, double cenaPoNoci, DateTime vremePrijave, DateTime vremeOdjave, StatusApartmana statusApartmana, List<SadrzajApartmana> sadrzaj, List<Rezervacija> rezervacije)
        {
            Tip = tip;
            BrojSoba = brojSoba;
            BrojGostiju = brojGostiju;
            Lokacija = lokacija;
            PocetniDatumIzdavanja = DateTime.Now;
            KrajnjiDatumIzdavanja = DateTime.Now;

            Domacin = domacin;
            Komentari = komentari;
            Slike = slike;
            CenaPoNoci = cenaPoNoci;
            VremePrijave = vremePrijave;
            VremeOdjave = vremeOdjave;
            StatusApartmana = statusApartmana;
            Sadrzaj = sadrzaj;
            Rezervacije = rezervacije;
        }
    }
}