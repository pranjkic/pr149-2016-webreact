﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Domacin : Korisnik
    {
        public int Id { get; set; }
        public ICollection<Apartman> ApartmaniZaIzdavanje { get; set; }

        public Domacin()
        {
            ApartmaniZaIzdavanje = new List<Apartman>();
        }

        public Domacin(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga) : base(korisnickoIme, lozinka, ime, prezime, pol, uloga)
        {
            ApartmaniZaIzdavanje = new List<Apartman>();
        }

        public Domacin(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga, List<Apartman> apartmaniZaIzdavanje) : base(korisnickoIme, lozinka, ime, prezime, pol, uloga)
        {
            ApartmaniZaIzdavanje = apartmaniZaIzdavanje;
        }

        //public override string ToString()
        //{
        //    string apps = "";
        //    if (ApartmaniZaIzdavanje.Count > 0)
        //    {
        //        foreach (Apartman app in ApartmaniZaIzdavanje)
        //            apps += "app_";
        //    }

        //    return KorisnickoIme + "|" + Lozinka + "|" + Ime + "|" + Prezime + "|" + Pol + "|Domacin|"+apps;
        //}

    }
}