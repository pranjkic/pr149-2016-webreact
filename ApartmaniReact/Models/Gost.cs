﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Gost : Korisnik
    {
        public int Id { get; set; }
        public ICollection<Apartman> IznajmljeniApartmani { get; set; }
        public ICollection<Rezervacija> RezervacijeGosta { get; set; }

        public Gost()
        {
            IznajmljeniApartmani = new List<Apartman>();
            RezervacijeGosta = new List<Rezervacija>();
        }

        public Gost(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga) : base(korisnickoIme, lozinka, ime, prezime, pol, uloga)
        {
            IznajmljeniApartmani = new List<Apartman>();
            RezervacijeGosta = new List<Rezervacija>();
        }

        public Gost(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga, List<Apartman> iznajmljeniApartmani, List<Rezervacija> rezervacije) : base(korisnickoIme, lozinka, ime, prezime, pol, uloga)
        {
            IznajmljeniApartmani = iznajmljeniApartmani;
            RezervacijeGosta = rezervacije;
        }

        //public override string ToString()
        //{
        //    return KorisnickoIme + "|" + Lozinka + "|" + Ime + "|" + Prezime + "|" + Pol + "|Gost\r\n";
        //}
    }
}