﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class KomentarApartmana
    {
        public int Id { get; set; }

        [ForeignKey("Gost"), Column("GostId")]
        public int GostId { get; set; }
        public Gost Gost { get; set; }

        [ForeignKey("Apartman"), Column("ApartmanId")]
        public int ApartmanId { get; set; }
        public Apartman Apartman { get; set; }

        public string Tekst { get; set; }
        public int Ocena { get; set; }

        public StatusKomentara Status { get; set; }

        public KomentarApartmana()
        {
            Gost = new Gost();
            Apartman = new Apartman();
        }

        public KomentarApartmana(Gost gost, Apartman apartman, string tekst, int ocena)
        {
            Gost = gost;
            Apartman = apartman;
            Tekst = tekst;
            Ocena = ocena;
        }
    }
}