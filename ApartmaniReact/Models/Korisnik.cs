﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Korisnik
    {
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
        public KorisnickeUloge Uloga { get; set; }
        public bool Blokiran { get; set; }

        public Korisnik()
        {
            Blokiran = false;
        }

        public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, string pol, KorisnickeUloge uloga)
        {
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Uloga = uloga;
            Blokiran = false;
        }
    }
}