﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Lokacija
    {
        public int Id { get; set; }
        public double GeografskaSirina { get; set; }
        public double GeografskaDuzina { get; set; }
        public string Drzava { get; set; }

        [ForeignKey("Adresa"), Column("AddrId")]
        public int AddrId { get; set; }
        public Adresa Adresa { get; set; }

        public Lokacija()
        {
            Adresa = new Adresa();
        }

        public Lokacija(double geografskaSirina, double geografskaDuzina, Adresa adresa)
        {
            GeografskaSirina = geografskaSirina;
            GeografskaDuzina = geografskaDuzina;
            Adresa = adresa;
        }

        //public override string ToString()
        //{
        //    return "Lokacija - " + GeografskaSirina + " " + GeografskaDuzina + " " +Adresa.Ulica + " " +Adresa.NaseljenoMesto + " " +Adresa.PostanskiBroj;
        //}
    }
}