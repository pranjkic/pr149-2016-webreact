﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class PraznicniDatum
    {
        public int Id { get; set; }
        public DateTime Datum { get; set; }
    }
}