﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Rezervacija
    {
        public int Id { get; set; }
        [ForeignKey("Apartman"), Column("AppId")]
        public int AppId { get; set; }
        public Apartman Apartman { get; set; }

        public DateTime PocetniDatum { get; set; }
        public int BrojNocenja { get; set; }
        public double UkupnaCena { get; set; }

        [ForeignKey("Gost"), Column("GostId")]
        public int GostId { get; set; }
        public Gost Gost { get; set; }

        public StatusRezervacije Status { get; set; }

        public DateTime KrajnjiDatum { get; set; }

        public Rezervacija(){ }

        public Rezervacija(Apartman apartman, DateTime pocetniDatum, int brojNocenja, double ukupnaCena, Gost gost, StatusRezervacije status)
        {
            Apartman = apartman;
            PocetniDatum = pocetniDatum;
            BrojNocenja = brojNocenja;
            UkupnaCena = ukupnaCena;
            Gost = gost;
            Status = status;
        }
    }
}