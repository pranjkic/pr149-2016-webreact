﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Sadrzaj
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public bool Obrisan { get; set; }

        public Sadrzaj() { }
    }
}