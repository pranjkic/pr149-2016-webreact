﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class SadrzajApartmana
    {
        public int Id { get; set; }
        public int SadrzajId { get; set; }
        public string Naziv { get; set; }
        public bool Obrisan { get; set; }

        public SadrzajApartmana() { }

        public SadrzajApartmana(int sadrzajId, string naziv)
        {
            SadrzajId = sadrzajId;
            Naziv = naziv;
        }
    }
}