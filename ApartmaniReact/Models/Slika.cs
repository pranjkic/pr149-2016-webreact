﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Slika
    {
        public int Id { get; set; }
        public string Photo { get; set; }

        public Slika() { }

        public Slika(string url)
        {
            Photo = url;
        }
    }
}