﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public enum StatusRezervacije
    {
        Kreirana,
        Odbijena,
        Odustanak,
        Prihvacena,
        Zavrsena
    }
}