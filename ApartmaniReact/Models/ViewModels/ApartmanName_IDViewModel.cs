﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class ApartmanName_IDViewModel
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
    }
}