﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class ApartmanViewModel
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Tip { get; set; }
        
        public int BrojSoba { get; set; }
        public int BrojGostiju { get; set; }        
        
        //LOKACIJA
        
        public int GeogSirina { get; set; }
        public int GeogDuzina { get; set; }
        public string Adresa { get; set; }
        public string Mesto { get; set; }
        public int PostanskiBroj { get; set; }

        //DATETIME POLJA
        public DateTime PocetniDatumIzdavanja { get; set; }
        public DateTime KrajnjiDatumIzdavanja { get; set; } 
        public string PocetniDatumIzdavanjaStr { get; set; }
        public string KrajnjiDatumIzdavanjaStr { get; set; }

        public List<KomentarApartmanaViewModel> Komentari { get; set; }      
        
        public string[] Slike { get; set; }
        public double CenaPoNoci { get; set; }
        
        //DATETIME POLJA
        public string VremePrijave { get; set; }
        public string VremeOdjave { get; set; }

        public string StatusApartmana { get; set; }
        
        public List<SadrzajApartmana> Sadrzaj { get; set; }
        public List<RezervacijaViewModel> Rezervacije { get; set; }

        public List<Sadrzaj> DostupanSadrzaj { get; set; }
        public string[] ZauzetiDatumi { get; set; }
    }
}