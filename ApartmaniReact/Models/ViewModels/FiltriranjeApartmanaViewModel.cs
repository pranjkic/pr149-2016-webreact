﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class FiltriranjeApartmanaViewModel
    {
        public string ParametarFiltriranja { get; set; }
        public string[] Sadrzaj { get; set; }
        public string TipApartmana { get; set; }
        public string Status { get; set; }
        public string KorisnickoImeKorisnika { get; set; }
    }
}