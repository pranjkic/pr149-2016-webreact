﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class IzmenaApartmanaViewModel
    {
        public string KorisnickoImeDomacina { get; set; }
        public string Naziv { get; set; }

        [Required]
        public string Tip { get; set; }

        [Required]
        [Range(0, 10)]
        public int BrojSoba { get; set; }

        [Required]
        [Range(0, 30)]
        public int BrojGostiju { get; set; }

        [Required]
        [Range(100, 100000000)]
        public double CenaPoNoci { get; set; }

        [Required]
        public DateTime VremePrijave { get; set; }

        [Required]
        public DateTime VremeOdjave { get; set; }

        public string[] Sadrzaj { get; set; }
        public string Status { get; set; }
        public string[] Slike { get; set; }
    }
}