﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class KomentarApartmanaViewModel
    {
        public int Id { get; set; }
        public string KorisnickoIme { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Tekst { get; set; }

        [Required]
        [Range(0, 10)]
        public int Ocena { get; set; }
        public string NazivApartmana { get; set; }
        public string Status { get; set; }

        //public override string ToString()
        //{
        //    return KorisnickoIme + " - "+Tekst + " "+Ocena;
        //}
    }
}