﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class KorisnikViewModel
    {
        public int Id { get; set; }
        public string KorisnickoImeUlogovanogKorisnika { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
        public string Uloga { get; set; }
        public bool Blokiran { get; set; }

        public KorisnikViewModel() { }

        public KorisnikViewModel(int id, string korisnickoIme, string ime, string prezime, string pol, string uloga)
        {
            Id = id;
            KorisnickoIme = korisnickoIme;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            Uloga = uloga;
        }
    }
}