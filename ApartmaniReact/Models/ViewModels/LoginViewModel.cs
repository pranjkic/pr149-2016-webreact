﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class LoginViewModel
    {
        //[Required]
        //[MinLength(4)]
        //[MaxLength(20)]
        public string KorisnickoIme { get; set; }

        //[Required]
        //[MinLength(4)]
        //[MaxLength(20)]
        public string Lozinka { get; set; }
    }
}