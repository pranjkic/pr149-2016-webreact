﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmaniReact.Models.ViewModels
{
    public class RegisterUserViewModel
    {
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
    }
}