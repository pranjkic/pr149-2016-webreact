﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models.ViewModel
{
    public class RezervacijaViewModel
    {
        public int Id { get; set; }
        public string PocetniDatum { get; set; }
        public string KrajnjiDatum { get; set; }
        public int BrojNocenja { get; set; }
        public double UkupnaCena { get; set; }
        //GOST
        public string KorisnickoIme { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }

        public string Status { get; set; }
        public string NazivApartmana { get; set; }
        public string KorisnickoImeDomacina { get; set; }
        public bool ObrisanApartman { get; set; }
        public bool BlokiranDomacin { get; set; }
        public bool BlokiranGost { get; set; }
        public bool StatusApartmana { get; set; }
    }
}