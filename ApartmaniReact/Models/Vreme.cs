﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjekatWEB.Models
{
    public class Vreme
    {
        public int Id { get; set; }
        public string vreme { get; set; }

        public Vreme(DateTime unetoVreme)
        {
            this.vreme = unetoVreme.ToString();
        }

        public Vreme() { }
    }
}